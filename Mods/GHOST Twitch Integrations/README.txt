Several new twitch integration commands for 7 Days to Die.
Created by xooGHOSToox(twitch) for Alpha 20.5

Commands are as follows:
#spawn_army (Spawns in two soliders and one demo)
#spawn_patrol (Spawns in three cops)
#spawn_friends (Spawns in five normal zombies)
#spawn_birds (Spawns in five vultures)
#spawn_gnats (Spawns in eight annoying gnats/custom tiny vultures)
#spawn_infested (Spawns in five snakes)
#spawn_anaconda (Spawns in one big snake)
#spawn_wolfpack (Spawns in three wolves)
#spawn_bikerclub (Spawns in one radiated biker and two normal bikers)
#spawn_spiderweb (Spawns in three spider zombies)
#poop (Gives dysentery to everyone in the party)
#brokenleg (Gives a broken leg to everyone in the party)
#zombieraid (Spawns in three waves of five zombies, starting with normal, then feral, then radiated)
#boost_xp (Boost player xp for five minutes)